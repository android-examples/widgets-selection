package br.com.wxu.widgetsselection

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private val textResult by lazy  { findViewById<TextView>(R.id.text_result)  }
    private val checkboxAndroidBasic by lazy  { findViewById<CheckBox>(R.id.checkbox_android_basic)  }
    private val checkboxAndroidMiddle by lazy  { findViewById<CheckBox>(R.id.checkbox_android_middle)  }
    private val checkboxAndroidAdvanced by lazy  { findViewById<CheckBox>(R.id.checkbox_android_advanced)  }
    private val radioGroupInfo by lazy  { findViewById<RadioGroup>(R.id.radio_group_info)  }
    private val buttonOK by lazy { findViewById<Button>(R.id.button_ok) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttonOK.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        var result = "Courses: "

        if(checkboxAndroidBasic.isChecked) {
            result += "${checkboxAndroidBasic.text}, "
        }

        if(checkboxAndroidMiddle.isChecked) {
            result += "${checkboxAndroidMiddle.text}, "
        }

        if(checkboxAndroidAdvanced.isChecked) {
            result += "${checkboxAndroidAdvanced.text}, "
        }

        if(radioGroupInfo.checkedRadioButtonId != -1) {
            val radioButtonSelected = findViewById<RadioButton>(radioGroupInfo.checkedRadioButtonId)
            result += "Receive more information? ${radioButtonSelected.text}"
        }

        textResult.text = result
    }
}

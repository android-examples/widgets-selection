package br.com.wxu.widgetsselection

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Spinner
import android.widget.TextView

class ListViewSpinnerActivity : AppCompatActivity(), View.OnClickListener {

    private val spinnerAreaActivity by lazy { findViewById<Spinner>(R.id.spinner_area) }
    private val listSkills by lazy { findViewById<ListView>(R.id.list_skills) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_view_spinner)

        val skills = listOf("Development", "Security Information", "DevOps")
        val adapterSkills = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, skills)

        val areaActivity = listOf<String>()
        val adapterAreaActivity = ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, areaActivity)

        spinnerAreaActivity.adapter = adapterAreaActivity
        listSkills.adapter = adapterSkills
    }

    override fun onClick(view: View?) {
        when(view?.id) {
            R.id.button_select -> {
                val textResult = findViewById<TextView>(R.id.text_area_selected)
                textResult.text = "Selected area: ${spinnerAreaActivity.selectedItem}"
            }
        }

    }
}
